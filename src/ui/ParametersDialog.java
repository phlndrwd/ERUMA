/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * OptionsDialog.java
 *
 * This is the parameter configuration dialog for experimentation with the model
 * setup.
 *
 *  @author     Phil Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui;                             // Part of the user interface package

import java.awt.Frame;                  // Handles a reference to the parent
import java.awt.event.WindowAdapter;    // For handling window events
import java.awt.event.WindowEvent;      // For generating window events
import javax.swing.JDialog;             // For extension of JDialog component
import javax.swing.JSpinner;            // The JSpinner input widget
import javax.swing.SpinnerModel;        // Handles user input
import javax.swing.SpinnerNumberModel;  // Uses a numerical extension
import param.Parameters;

@SuppressWarnings( "serial" )
public class ParametersDialog extends JDialog {

    /*
     * Constructor
     * @param parent A reference to the parent
     * @param modal Whether this window is always on top of others
     */
    public ParametersDialog( Frame parent, boolean modal ) {

        super( parent, modal );               // Call constructor of parent
        setLocationRelativeTo( null );        // Centre on screen
        initComponents();                   // Initialise the interface
        saveSettings();                     // Store variables

        // Add a window listener so that the close button acts as cancelling
        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                cancel();
            }
        } );
    }

    /*
     * Stores variables as configured on the interface
     */
    public final void saveSettings() {

        Parameters.setResourceMaximum( Integer.parseInt( ( mSpinnerResourceMaximum.getValue().toString() ) ) );
        Parameters.setResourceMinimum( Integer.parseInt( ( mSpinnerResourceMinimum.getValue().toString() ) ) );
        Parameters.setCarryingCapacity( Integer.parseInt( ( mSpinnerCarryingCapacity.getValue().toString() ) ) );
        Parameters.setPopulationMinimum( Integer.parseInt( ( mSpinnerPopulationMinimum.getValue().toString() ) ) );
        Parameters.setForcingStrength( Double.parseDouble( mSpinnerForcingStrength.getValue().toString() ) );
        Parameters.setNicheConstructionStrength( Double.parseDouble( mSpinnerNicheConstruction.getValue().toString() ) );
        Parameters.setMaximumTime( Integer.parseInt( mSpinnerMaximumTime.getValue().toString() ) );
        Parameters.setIsFixedPopulation( mRadioFixedPopulation.isSelected() );
        Parameters.setLowerHabitableBound( Integer.parseInt( mSpinnerLowerHabitableBound.getValue().toString() ) );
        Parameters.setUpperHabitableBound( Integer.parseInt( mSpinnerUpperHabitableBound.getValue().toString() ) );
        Parameters.setMutationRate( 0, Double.parseDouble( mSpinnerAMutationProbability.getValue().toString() ) );
        Parameters.setMutationRate( 1, Double.parseDouble( mSpinnerEMutationProbability.getValue().toString() ) );
        Parameters.setMutationStandardDeviation( 0, Double.parseDouble( mSpinnerAMuStandardDeviation.getValue().toString() ) );
        Parameters.setMutationStandardDeviation( 1, Double.parseDouble( mSpinnerEMuStandardDeviation.getValue().toString() ) );
        Parameters.setDeathRate( Double.parseDouble( mSpinnerDeathRate.getValue().toString() ) );
        Parameters.setFitnessFunctionWidth( Double.parseDouble( mSpinnerParabolaWidth.getValue().toString() ) );
        Parameters.setFixedImpact( mRadioFixedImpact.isSelected() );
        Parameters.recalculateParameters();
    }

    /*
     * Called if the window is closed or if 'cancel' is clicked, discards changes
     */
    public void cancel() {
        setVisible( false );
        mSpinnerResourceMaximum.setValue( Parameters.getResourceMaximum() );
        mSpinnerResourceMinimum.setValue( Parameters.getResourceMinimum() );
        mSpinnerCarryingCapacity.setValue( Parameters.getCarryingCapacity() );
        mSpinnerPopulationMinimum.setValue( Parameters.getPopulationMinimum() );
        mSpinnerForcingStrength.setValue( Parameters.getForcingStrength() );
        mSpinnerNicheConstruction.setValue( Parameters.getNicheConstructionStrength() );
        mSpinnerMaximumTime.setValue( Parameters.getMaximumTime() );
        mRadioFixedPopulation.setSelected( Parameters.isFixedPopulation() );
        mSpinnerLowerHabitableBound.setValue( Parameters.getLowerHabitableBound() );
        mSpinnerUpperHabitableBound.setValue( Parameters.getUpperHabitableBound() );
        mSpinnerAMutationProbability.setValue( Parameters.getMutationRate( 0 ) );
        mSpinnerEMutationProbability.setValue( Parameters.getMutationRate( 1 ) );
        mSpinnerAMuStandardDeviation.setValue( Parameters.getMutationStandardDeviation( 0 ) );
        mSpinnerEMuStandardDeviation.setValue( Parameters.getMutationStandardDeviation( 1 ) );
        mSpinnerDeathRate.setValue( Parameters.getDeathRate() );
        mSpinnerParabolaWidth.setValue( Parameters.getFitnessFunctionWidth() );
        mRadioFixedImpact.setSelected( Parameters.isFixedImpact() );
    }

    /*
     * Called if 'OK' is clicked and saves settings.
     */
    public void ok() {
        setVisible( false );
        saveSettings();
    }

    /*
     * Called if 'Apply' is clicked, saves settings but keeps dialog visible
     */
    public void apply() {
        saveSettings();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupImpact = new javax.swing.ButtonGroup();
        groupPopulation = new javax.swing.ButtonGroup();
        mPanelParameters = new javax.swing.JPanel();
        mLabelResourceMinimum = new javax.swing.JLabel();
        mLabelResourceMaximum = new javax.swing.JLabel();
        mLabelCarryingCapacity = new javax.swing.JLabel();
        mLabelPopulationMinimum = new javax.swing.JLabel();
        mLabelForcingStrength = new javax.swing.JLabel();
        mLabelNicheConstruction = new javax.swing.JLabel();
        mLabelMaximumTime = new javax.swing.JLabel();
        SpinnerModel modelRMin = new SpinnerNumberModel(0, -50, 50, 1);
        mSpinnerResourceMinimum = new JSpinner(modelRMin);
        SpinnerModel modelRMax = new SpinnerNumberModel(100, 50, 150, 1);
        mSpinnerResourceMaximum = new JSpinner(modelRMax);
        SpinnerModel modelCapacity = new SpinnerNumberModel(1000, 0, 2000, 1);
        mSpinnerCarryingCapacity = new JSpinner(modelCapacity);
        SpinnerModel modelPopMin = new SpinnerNumberModel(10, 0, 2000, 1);
        mSpinnerPopulationMinimum = new JSpinner(modelPopMin);
        SpinnerModel modelExtForcing = new SpinnerNumberModel(0.01, 0.01, 1, 0.01);
        mSpinnerForcingStrength = new JSpinner(modelExtForcing);
        SpinnerModel modelNiche = new SpinnerNumberModel(5, 1, 100, 1);
        mSpinnerNicheConstruction = new JSpinner(modelNiche);
        SpinnerModel modelMaxTime = new SpinnerNumberModel(100000, 10000, 1000000, 1);
        mSpinnerMaximumTime = new JSpinner(modelMaxTime);
        mRadioFixedPopulation = new javax.swing.JRadioButton();
        mRadioVariablePopulation = new javax.swing.JRadioButton();
        mPanelOrganism = new javax.swing.JPanel();
        mLabelLowerHabitableBound = new javax.swing.JLabel();
        mLabelUpperHabitableBound = new javax.swing.JLabel();
        mLabelAMutationProbability = new javax.swing.JLabel();
        mLabelAMuStandardDeviation = new javax.swing.JLabel();
        mLabelEMutationProbability = new javax.swing.JLabel();
        mLabelEMuStandardDeviation = new javax.swing.JLabel();
        mLabelDeathRate = new javax.swing.JLabel();
        mLabelParabolaWidth = new javax.swing.JLabel();
        SpinnerModel modelBoundMin = new SpinnerNumberModel(15, 0, 50, 1);
        mSpinnerLowerHabitableBound = new JSpinner(modelBoundMin);
        SpinnerModel modelBoundMax = new SpinnerNumberModel(85, 50, 100, 1);
        mSpinnerUpperHabitableBound = new JSpinner(modelBoundMax);
        SpinnerModel modelAMut = new SpinnerNumberModel(0.1, 0, 1, 0.01);
        mSpinnerAMutationProbability = new JSpinner(modelAMut);
        SpinnerModel modelAMutStandDev = new SpinnerNumberModel(0.05, 0, 1, 0.01);
        mSpinnerAMuStandardDeviation = new JSpinner(modelAMutStandDev);
        SpinnerModel modelEMut = new SpinnerNumberModel(0.1, 0, 1, 0.01);
        mSpinnerEMutationProbability = new JSpinner(modelEMut);
        SpinnerModel modelEMutStandDev = new SpinnerNumberModel(0.05, 0, 1, 0.01);
        mSpinnerEMuStandardDeviation = new JSpinner(modelEMutStandDev);
        SpinnerModel modelDeath = new SpinnerNumberModel(0.005, 0.001, 0.01, 0.001);
        mSpinnerDeathRate = new JSpinner(modelDeath);
        SpinnerModel modelLamda = new SpinnerNumberModel(5, 0.1, 10, 0.1);
        mSpinnerParabolaWidth = new JSpinner(modelLamda);
        mRadioFixedImpact = new javax.swing.JRadioButton();
        mRadioContinuousImpact = new javax.swing.JRadioButton();
        mButtonOK = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonApply = new javax.swing.JButton();

        groupImpact.add(mRadioFixedImpact);
        groupImpact.add(mRadioContinuousImpact);

        groupPopulation.add(mRadioFixedPopulation);
        groupPopulation.add(mRadioVariablePopulation);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Parameters");
        setResizable(false);

        mPanelParameters.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Parameters"));

        mLabelResourceMinimum.setText("Resource (R) Min:");
        mLabelResourceMinimum.setPreferredSize(new java.awt.Dimension(86, 17));

        mLabelResourceMaximum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelResourceMaximum.setText("Max:");
        mLabelResourceMaximum.setPreferredSize(new java.awt.Dimension(24, 17));

        mLabelCarryingCapacity.setText("Carrying Capacity (K):");
        mLabelCarryingCapacity.setPreferredSize(new java.awt.Dimension(143, 17));

        mLabelPopulationMinimum.setText("<html>Minimum Population Size (n<sub>min</sub>):</html>");
        mLabelPopulationMinimum.setPreferredSize(new java.awt.Dimension(143, 17));

        mLabelForcingStrength.setText("External Forcing Strength (β):");
        mLabelForcingStrength.setPreferredSize(new java.awt.Dimension(143, 17));

        mLabelNicheConstruction.setText("Niche Construction  x 10E-5 (α):");
        mLabelNicheConstruction.setPreferredSize(new java.awt.Dimension(130, 17));

        mLabelMaximumTime.setText("<html>Maximum Simulation Time (t<sub>max</sub>):</html>");

        mRadioFixedPopulation.setSelected(true);
        mRadioFixedPopulation.setText("Fixed Population");
        mRadioFixedPopulation.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioVariablePopulation.setText("Variable Population");
        mRadioVariablePopulation.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelParametersLayout = new javax.swing.GroupLayout(mPanelParameters);
        mPanelParameters.setLayout(mPanelParametersLayout);
        mPanelParametersLayout.setHorizontalGroup(
            mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelParametersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelParametersLayout.createSequentialGroup()
                        .addComponent(mRadioFixedPopulation)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mRadioVariablePopulation, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mPanelParametersLayout.createSequentialGroup()
                        .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mPanelParametersLayout.createSequentialGroup()
                                .addComponent(mLabelResourceMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mSpinnerResourceMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mLabelResourceMaximum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(mLabelCarryingCapacity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelMaximumTime)
                            .addComponent(mLabelNicheConstruction, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelForcingStrength, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelPopulationMinimum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mSpinnerPopulationMinimum)
                            .addComponent(mSpinnerForcingStrength, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerNicheConstruction, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerMaximumTime, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerCarryingCapacity, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerResourceMaximum, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        mPanelParametersLayout.setVerticalGroup(
            mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelParametersLayout.createSequentialGroup()
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerResourceMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelResourceMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelResourceMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerResourceMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelCarryingCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerCarryingCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mSpinnerPopulationMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelPopulationMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelForcingStrength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerForcingStrength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelNicheConstruction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerNicheConstruction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelMaximumTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerMaximumTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelParametersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mRadioFixedPopulation)
                    .addComponent(mRadioVariablePopulation))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelOrganism.setBorder(javax.swing.BorderFactory.createTitledBorder("Organism Parameters"));

        mLabelLowerHabitableBound.setText("Habitable Range Min:");
        mLabelLowerHabitableBound.setPreferredSize(new java.awt.Dimension(86, 17));

        mLabelUpperHabitableBound.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelUpperHabitableBound.setText("Max:");
        mLabelUpperHabitableBound.setPreferredSize(new java.awt.Dimension(24, 17));

        mLabelAMutationProbability.setText("A Mutation Probability:");
        mLabelAMutationProbability.setPreferredSize(new java.awt.Dimension(156, 17));

        mLabelAMuStandardDeviation.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelAMuStandardDeviation.setText("<html>σ<sub>A</sub>:</html>");

        mLabelEMutationProbability.setText("E Mutation Probability:");
        mLabelEMutationProbability.setPreferredSize(new java.awt.Dimension(156, 17));

        mLabelEMuStandardDeviation.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelEMuStandardDeviation.setText("<html>σ<sub>E</sub>:</html>");

        mLabelDeathRate.setText("Death Rate:");
        mLabelDeathRate.setPreferredSize(new java.awt.Dimension(156, 17));

        mLabelParabolaWidth.setText("Parabola Width:");
        mLabelParabolaWidth.setPreferredSize(new java.awt.Dimension(156, 17));

        mRadioFixedImpact.setSelected(true);
        mRadioFixedImpact.setText("Fixed Impact");
        mRadioFixedImpact.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mRadioFixedImpact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mRadioFixedImpactActionPerformed(evt);
            }
        });

        mRadioContinuousImpact.setText("Continuous Impact");
        mRadioContinuousImpact.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelOrganismLayout = new javax.swing.GroupLayout(mPanelOrganism);
        mPanelOrganism.setLayout(mPanelOrganismLayout);
        mPanelOrganismLayout.setHorizontalGroup(
            mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelOrganismLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelOrganismLayout.createSequentialGroup()
                        .addComponent(mRadioFixedImpact)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mRadioContinuousImpact)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(mPanelOrganismLayout.createSequentialGroup()
                        .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mLabelParabolaWidth, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelDeathRate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelAMutationProbability, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelEMutationProbability, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelLowerHabitableBound, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                            .addComponent(mSpinnerEMutationProbability, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerAMutationProbability, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerLowerHabitableBound, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerParabolaWidth))
                        .addGap(7, 7, 7)
                        .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mLabelUpperHabitableBound, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                            .addComponent(mLabelAMuStandardDeviation)
                            .addComponent(mLabelEMuStandardDeviation))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mSpinnerUpperHabitableBound)
                            .addComponent(mSpinnerAMuStandardDeviation)
                            .addComponent(mSpinnerEMuStandardDeviation))))
                .addContainerGap())
        );
        mPanelOrganismLayout.setVerticalGroup(
            mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelOrganismLayout.createSequentialGroup()
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelLowerHabitableBound, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerLowerHabitableBound, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelUpperHabitableBound, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerUpperHabitableBound, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelAMuStandardDeviation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerAMuStandardDeviation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelAMutationProbability, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerAMutationProbability, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelEMutationProbability, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerEMutationProbability, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelEMuStandardDeviation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerEMuStandardDeviation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelDeathRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelParabolaWidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerParabolaWidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mRadioFixedImpact)
                    .addComponent(mRadioContinuousImpact))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelParameters, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mPanelOrganism, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(mButtonOK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mPanelParameters, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mPanelOrganism, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mButtonApply)
                    .addComponent(mButtonCancel)
                    .addComponent(mButtonOK))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    private void mRadioFixedImpactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mRadioFixedImpactActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mRadioFixedImpactActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                new ParametersDialog( new javax.swing.JFrame(), true ).setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup groupImpact;
    private javax.swing.ButtonGroup groupPopulation;
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JLabel mLabelAMuStandardDeviation;
    private javax.swing.JLabel mLabelAMutationProbability;
    private javax.swing.JLabel mLabelCarryingCapacity;
    private javax.swing.JLabel mLabelDeathRate;
    private javax.swing.JLabel mLabelEMuStandardDeviation;
    private javax.swing.JLabel mLabelEMutationProbability;
    private javax.swing.JLabel mLabelForcingStrength;
    private javax.swing.JLabel mLabelLowerHabitableBound;
    private javax.swing.JLabel mLabelMaximumTime;
    private javax.swing.JLabel mLabelNicheConstruction;
    private javax.swing.JLabel mLabelParabolaWidth;
    private javax.swing.JLabel mLabelPopulationMinimum;
    private javax.swing.JLabel mLabelResourceMaximum;
    private javax.swing.JLabel mLabelResourceMinimum;
    private javax.swing.JLabel mLabelUpperHabitableBound;
    private javax.swing.JPanel mPanelOrganism;
    private javax.swing.JPanel mPanelParameters;
    private javax.swing.JRadioButton mRadioContinuousImpact;
    private javax.swing.JRadioButton mRadioFixedImpact;
    private javax.swing.JRadioButton mRadioFixedPopulation;
    private javax.swing.JRadioButton mRadioVariablePopulation;
    private javax.swing.JSpinner mSpinnerAMuStandardDeviation;
    private javax.swing.JSpinner mSpinnerAMutationProbability;
    private javax.swing.JSpinner mSpinnerCarryingCapacity;
    private javax.swing.JSpinner mSpinnerDeathRate;
    private javax.swing.JSpinner mSpinnerEMuStandardDeviation;
    private javax.swing.JSpinner mSpinnerEMutationProbability;
    private javax.swing.JSpinner mSpinnerForcingStrength;
    private javax.swing.JSpinner mSpinnerLowerHabitableBound;
    private javax.swing.JSpinner mSpinnerMaximumTime;
    private javax.swing.JSpinner mSpinnerNicheConstruction;
    private javax.swing.JSpinner mSpinnerParabolaWidth;
    private javax.swing.JSpinner mSpinnerPopulationMinimum;
    private javax.swing.JSpinner mSpinnerResourceMaximum;
    private javax.swing.JSpinner mSpinnerResourceMinimum;
    private javax.swing.JSpinner mSpinnerUpperHabitableBound;
    // End of variables declaration//GEN-END:variables
}
