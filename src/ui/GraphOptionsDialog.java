/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GraphDialog.java
 *
 * This is the graph options dialog, from which graph colours can be chosen.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui;                             // Part of the user interface class

import java.awt.Color;                  // To handle colours
import java.awt.Frame;                  // To handle the reference to the parent
import java.awt.event.WindowAdapter;    // For handling window events
import java.awt.event.WindowEvent;      // For generating window events
import javax.swing.JButton;             // To create a temporary button
import javax.swing.JDialog;             // For extension of JDialog component
import javax.swing.JOptionPane;         // For error dialog
import param.GraphOptions;

@SuppressWarnings( "serial" )
public class GraphOptionsDialog extends JDialog {

    private ColourChooserDialog mColourChooserDialog;       // The colour chooser object

    /*
     * Constructor
     * @param parent A reference to the parent
     * @param modal Whether this window is always on top of others
     */
    public GraphOptionsDialog( Frame parent, boolean modal ) {

        super( parent, modal );           // Call constructor of the parent
        setLocationRelativeTo( null );    // Centre on screen
        initComponents();               // Initialise the interface
        initColours();                  // Initialise the colours
        savePlotData();                 // Save the variables by default

        // Add a window listener so that the close button acts as cancelling
        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                cancel();
            }
        } );
    }

    /*
     * Used to call the colour chooser and set the button colour
     * @param source A reference to the button that called this method
     */
    public void configColour( Object source ) {
        JButton temp = ( JButton )source;

        mColourChooserDialog.setVisible( true );

        if( mColourChooserDialog.getColour() != null ) {
            temp.setBackground( mColourChooserDialog.getColour() );
        }
    }

    /*
     * General save method
     */
    public void saveSettings() {
        saveColours();
        savePlotData();
    }

    /*
     * Save the graph plot data
     */
    public final void savePlotData() {

        // Ensure that at least one option is selected for the top graph
        int count = 0;
        if( mCheckForcing.isSelected() ) {
            count += 1;
        }
        if( mCheckResource.isSelected() ) {
            count += 1;
        }
        if( mCheckHabitableBounds.isSelected() ) {
            count += 1;
        }
        if( mCheckMeanAdaptation.isSelected() ) {
            count += 1;
        }
        if( count == 0 ) {
            JOptionPane.showMessageDialog( this, "You must select at least one variable to plot on the top graph. System will default to plot forcing (P).", "Error", JOptionPane.ERROR_MESSAGE );
            mCheckForcing.setSelected( true );
        }

        GraphOptions.setTopPlot( 0, mCheckForcing.isSelected() );
        GraphOptions.setTopPlot( 1, mCheckResource.isSelected() );
        GraphOptions.setTopPlot( 2, mCheckHabitableBounds.isSelected() );
        GraphOptions.setTopPlot( 3, mCheckMeanAdaptation.isSelected() );

        GraphOptions.setBottomPlot( 0, mRadioNicheConstruction.isSelected() );
        GraphOptions.setBottomPlot( 1, mRadioMeanFitness.isSelected() );
        GraphOptions.setBottomPlot( 2, mRadioEIncreaseAlleles.isSelected() );
        GraphOptions.setBottomPlot( 3, mRadioEDecreaseAllelesColour.isSelected() );
    }

    /*
     * Stores the colour information
     */
    public void saveColours() {

        GraphOptions.setTopLineColour( 0, mButtonForcingColour.getBackground() );
        GraphOptions.setTopLineColour( 1, mButtonResourceColour.getBackground() );
        GraphOptions.setTopLineColour( 2, mButtonHabitableBoundsColour.getBackground() );
        GraphOptions.setTopLineColour( 3, mButtonMeanAdaptationColour.getBackground() );

        GraphOptions.setBottomLineColour( 0, mButtonNicheConstructionColour.getBackground() );
        GraphOptions.setBottomLineColour( 1, mButtonMeanFitness.getBackground() );
        GraphOptions.setBottomLineColour( 2, mButtonEIncreaseAllelesColour.getBackground() );
        GraphOptions.setBottomLineColour( 3, mButtonEDecreaseAllelesColour.getBackground() );

        GraphOptions.setGraphColour( 0, mButtonPlotColour.getBackground() );
        GraphOptions.setGraphColour( 1, mButtonGraphLineColour.getBackground() );
    }

    /*
     * Called if the window is closed or if 'cancel' is clicked, discards changes
     */
    public void cancel() {

        resetColorButtons();
        resetPlotOptions();
        setVisible( false );
    }

    /*
     * Called if 'OK' is clicked and saves settings.
     */
    public void ok() {
        saveSettings();
        setVisible( false );
    }

    /*
     * Called if 'Apply' is clicked, saves settings but keeps dialog visible
     */
    public void apply() {
        saveSettings();
    }

    /*
     * Initialise the colour variables to their default values
     */
    public final void initColours() {

        mColourChooserDialog = new ColourChooserDialog( null, true );

        GraphOptions.setTopLineColour( 0, Color.red );
        GraphOptions.setTopLineColour( 1, new Color( 0, 128, 0 ) );
        GraphOptions.setTopLineColour( 2, Color.darkGray );
        GraphOptions.setTopLineColour( 3, Color.blue );

        GraphOptions.setBottomLineColour( 0, new Color( 64, 0, 128 ) );
        GraphOptions.setBottomLineColour( 1, new Color( 255, 128, 0 ) );
        GraphOptions.setBottomLineColour( 2, Color.black );
        GraphOptions.setBottomLineColour( 3, Color.black );

        GraphOptions.setGraphColour( 0, new Color( 240, 240, 240 ) );
        GraphOptions.setGraphColour( 1, Color.white );

        resetColorButtons();
    }

    /*
     * Restore the interface widgets to the set colour
     */
    public void resetColorButtons() {

        mButtonForcingColour.setBackground( GraphOptions.getTopLineColour( 0 ) );
        mButtonResourceColour.setBackground( GraphOptions.getTopLineColour( 1 ) );
        mButtonHabitableBoundsColour.setBackground( GraphOptions.getTopLineColour( 2 ) );
        mButtonMeanAdaptationColour.setBackground( GraphOptions.getTopLineColour( 3 ) );

        mButtonNicheConstructionColour.setBackground( GraphOptions.getBottomLineColour( 0 ) );
        mButtonMeanFitness.setBackground( GraphOptions.getBottomLineColour( 1 ) );
        mButtonEIncreaseAllelesColour.setBackground( GraphOptions.getBottomLineColour( 2 ) );
        mButtonEDecreaseAllelesColour.setBackground( GraphOptions.getBottomLineColour( 3 ) );

        mButtonPlotColour.setBackground( GraphOptions.getGraphColour( 0 ) );
        mButtonGraphLineColour.setBackground( GraphOptions.getGraphColour( 1 ) );
    }

    /*
     * Restore the interface widgets to the set value
     */
    public void resetPlotOptions() {
        mCheckForcing.setSelected( GraphOptions.getTopPlot( 0 ) );
        mCheckResource.setSelected( GraphOptions.getTopPlot( 1 ) );
        mCheckHabitableBounds.setSelected( GraphOptions.getTopPlot( 2 ) );
        mCheckMeanAdaptation.setSelected( GraphOptions.getTopPlot( 3 ) );

        mRadioNicheConstruction.setSelected( GraphOptions.getBottomPlot( 0 ) );
        mRadioMeanFitness.setSelected( GraphOptions.getBottomPlot( 1 ) );
        mRadioEIncreaseAlleles.setSelected( GraphOptions.getBottomPlot( 2 ) );
        mRadioEDecreaseAllelesColour.setSelected( GraphOptions.getBottomPlot( 3 ) );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupBottomY = new javax.swing.ButtonGroup();
        mButtonApply = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonOK = new javax.swing.JButton();
        mPanelTopPlots = new javax.swing.JPanel();
        mCheckForcing = new javax.swing.JCheckBox();
        mCheckResource = new javax.swing.JCheckBox();
        mButtonResourceColour = new javax.swing.JButton();
        mButtonForcingColour = new javax.swing.JButton();
        mCheckHabitableBounds = new javax.swing.JCheckBox();
        mButtonHabitableBoundsColour = new javax.swing.JButton();
        mCheckMeanAdaptation = new javax.swing.JCheckBox();
        mButtonMeanAdaptationColour = new javax.swing.JButton();
        mPanelBottomPlot = new javax.swing.JPanel();
        mRadioNicheConstruction = new javax.swing.JRadioButton();
        mRadioEIncreaseAlleles = new javax.swing.JRadioButton();
        mRadioMeanFitness = new javax.swing.JRadioButton();
        mButtonNicheConstructionColour = new javax.swing.JButton();
        mButtonMeanFitness = new javax.swing.JButton();
        mButtonEIncreaseAllelesColour = new javax.swing.JButton();
        mRadioEDecreaseAllelesColour = new javax.swing.JRadioButton();
        mButtonEDecreaseAllelesColour = new javax.swing.JButton();
        mPanelColourOptions = new javax.swing.JPanel();
        mLabelPlot = new javax.swing.JLabel();
        mButtonPlotColour = new javax.swing.JButton();
        mLabelGraphLine = new javax.swing.JLabel();
        mButtonGraphLineColour = new javax.swing.JButton();

        groupBottomY.add(mRadioNicheConstruction);
        groupBottomY.add(mRadioMeanFitness);
        groupBottomY.add(mRadioEIncreaseAlleles);
        groupBottomY.add(mRadioEDecreaseAllelesColour);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Graph Options");
        setResizable(false);

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        mPanelTopPlots.setBorder(javax.swing.BorderFactory.createTitledBorder("Top Plots"));

        mCheckForcing.setSelected(true);
        mCheckForcing.setText("Forcing (P)");
        mCheckForcing.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mCheckResource.setSelected(true);
        mCheckResource.setText("Resource (R)");
        mCheckResource.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mButtonResourceColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonResourceColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonResourceColourActionPerformed(evt);
            }
        });

        mButtonForcingColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonForcingColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonForcingColourActionPerformed(evt);
            }
        });

        mCheckHabitableBounds.setSelected(true);
        mCheckHabitableBounds.setText("Habitable Bounds");
        mCheckHabitableBounds.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mButtonHabitableBoundsColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonHabitableBoundsColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonHabitableBoundsColourActionPerformed(evt);
            }
        });

        mCheckMeanAdaptation.setSelected(true);
        mCheckMeanAdaptation.setText("Mean Adaptation (A)");
        mCheckMeanAdaptation.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mButtonMeanAdaptationColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonMeanAdaptationColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonMeanAdaptationColourActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mPanelTopPlotsLayout = new javax.swing.GroupLayout(mPanelTopPlots);
        mPanelTopPlots.setLayout(mPanelTopPlotsLayout);
        mPanelTopPlotsLayout.setHorizontalGroup(
            mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelTopPlotsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mCheckHabitableBounds, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mCheckMeanAdaptation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mCheckResource, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mCheckForcing, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelTopPlotsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonMeanAdaptationColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(mButtonHabitableBoundsColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mButtonResourceColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mButtonForcingColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        mPanelTopPlotsLayout.setVerticalGroup(
            mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelTopPlotsLayout.createSequentialGroup()
                .addGroup(mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mButtonMeanAdaptationColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(mPanelTopPlotsLayout.createSequentialGroup()
                        .addGroup(mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mCheckForcing)
                            .addComponent(mButtonForcingColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mButtonResourceColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mCheckResource))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelTopPlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mButtonHabitableBoundsColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mCheckHabitableBounds))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mCheckMeanAdaptation)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelBottomPlot.setBorder(javax.swing.BorderFactory.createTitledBorder("Bottom Plot"));

        mRadioNicheConstruction.setSelected(true);
        mRadioNicheConstruction.setText("Niche Construction");
        mRadioNicheConstruction.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioEIncreaseAlleles.setText("Number of E Individuals");
        mRadioEIncreaseAlleles.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioMeanFitness.setText("Mean Fitness (F)");
        mRadioMeanFitness.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mButtonNicheConstructionColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonNicheConstructionColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonNicheConstructionColourActionPerformed(evt);
            }
        });

        mButtonMeanFitness.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonMeanFitness.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonMeanFitnessActionPerformed(evt);
            }
        });

        mButtonEIncreaseAllelesColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonEIncreaseAllelesColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonEIncreaseAllelesColourActionPerformed(evt);
            }
        });

        mRadioEDecreaseAllelesColour.setText("Number of e Individuals");
        mRadioEDecreaseAllelesColour.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mButtonEDecreaseAllelesColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonEDecreaseAllelesColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonEDecreaseAllelesColourActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mPanelBottomPlotLayout = new javax.swing.GroupLayout(mPanelBottomPlot);
        mPanelBottomPlot.setLayout(mPanelBottomPlotLayout);
        mPanelBottomPlotLayout.setHorizontalGroup(
            mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelBottomPlotLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelBottomPlotLayout.createSequentialGroup()
                        .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mRadioMeanFitness, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mRadioNicheConstruction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mRadioEIncreaseAlleles))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mButtonNicheConstructionColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mButtonMeanFitness, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mButtonEIncreaseAllelesColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(mPanelBottomPlotLayout.createSequentialGroup()
                        .addComponent(mRadioEDecreaseAllelesColour)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonEDecreaseAllelesColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mPanelBottomPlotLayout.setVerticalGroup(
            mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelBottomPlotLayout.createSequentialGroup()
                .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mRadioNicheConstruction)
                    .addComponent(mButtonNicheConstructionColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mButtonMeanFitness, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mRadioMeanFitness))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mButtonEIncreaseAllelesColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mRadioEIncreaseAlleles))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelBottomPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mRadioEDecreaseAllelesColour)
                    .addComponent(mButtonEDecreaseAllelesColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelColourOptions.setBorder(javax.swing.BorderFactory.createTitledBorder("Colour Options"));

        mLabelPlot.setText("Plot Background:");

        mButtonPlotColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonPlotColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonPlotColourActionPerformed(evt);
            }
        });

        mLabelGraphLine.setText("Graph Line Background:");

        mButtonGraphLineColour.setPreferredSize(new java.awt.Dimension(16, 16));
        mButtonGraphLineColour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonGraphLineColourActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mPanelColourOptionsLayout = new javax.swing.GroupLayout(mPanelColourOptions);
        mPanelColourOptions.setLayout(mPanelColourOptionsLayout);
        mPanelColourOptionsLayout.setHorizontalGroup(
            mPanelColourOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelColourOptionsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelColourOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mLabelGraphLine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelPlot))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelColourOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mButtonGraphLineColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mButtonPlotColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        mPanelColourOptionsLayout.setVerticalGroup(
            mPanelColourOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelColourOptionsLayout.createSequentialGroup()
                .addGroup(mPanelColourOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mButtonPlotColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelPlot))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelColourOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mButtonGraphLineColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelGraphLine))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mPanelTopPlots, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mPanelBottomPlot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelColourOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(mButtonOK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(mPanelColourOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mButtonApply)
                            .addComponent(mButtonCancel)
                            .addComponent(mButtonOK)))
                    .addComponent(mPanelBottomPlot, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mPanelTopPlots, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mButtonEDecreaseAllelesColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonEDecreaseAllelesColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonEDecreaseAllelesColourActionPerformed

    private void mButtonMeanAdaptationColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonMeanAdaptationColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonMeanAdaptationColourActionPerformed

    private void mButtonGraphLineColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonGraphLineColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonGraphLineColourActionPerformed

    private void mButtonPlotColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonPlotColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonPlotColourActionPerformed

    private void mButtonEIncreaseAllelesColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonEIncreaseAllelesColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonEIncreaseAllelesColourActionPerformed

    private void mButtonMeanFitnessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonMeanFitnessActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonMeanFitnessActionPerformed

    private void mButtonNicheConstructionColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonNicheConstructionColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonNicheConstructionColourActionPerformed

    private void mButtonHabitableBoundsColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonHabitableBoundsColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonHabitableBoundsColourActionPerformed

    private void mButtonForcingColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonForcingColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonForcingColourActionPerformed

    private void mButtonResourceColourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonResourceColourActionPerformed
        configColour( evt.getSource() );
    }//GEN-LAST:event_mButtonResourceColourActionPerformed

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            public void run() {
                new ParametersDialog( new javax.swing.JFrame(), true ).setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup groupBottomY;
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonEDecreaseAllelesColour;
    private javax.swing.JButton mButtonEIncreaseAllelesColour;
    private javax.swing.JButton mButtonForcingColour;
    private javax.swing.JButton mButtonGraphLineColour;
    private javax.swing.JButton mButtonHabitableBoundsColour;
    private javax.swing.JButton mButtonMeanAdaptationColour;
    private javax.swing.JButton mButtonMeanFitness;
    private javax.swing.JButton mButtonNicheConstructionColour;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JButton mButtonPlotColour;
    private javax.swing.JButton mButtonResourceColour;
    private javax.swing.JCheckBox mCheckForcing;
    private javax.swing.JCheckBox mCheckHabitableBounds;
    private javax.swing.JCheckBox mCheckMeanAdaptation;
    private javax.swing.JCheckBox mCheckResource;
    private javax.swing.JLabel mLabelGraphLine;
    private javax.swing.JLabel mLabelPlot;
    private javax.swing.JPanel mPanelBottomPlot;
    private javax.swing.JPanel mPanelColourOptions;
    private javax.swing.JPanel mPanelTopPlots;
    private javax.swing.JRadioButton mRadioEDecreaseAllelesColour;
    private javax.swing.JRadioButton mRadioEIncreaseAlleles;
    private javax.swing.JRadioButton mRadioMeanFitness;
    private javax.swing.JRadioButton mRadioNicheConstruction;
    // End of variables declaration//GEN-END:variables
}
