/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Genotype.java
 *
 * This class represents the genetic makeup of the organisms. Consisting of two
 * doubles it can either be initialised randomly or the genetic information can
 * be passed to it upon constuction. This class also handles reproduction and
 * potential mutation.
 *
 * The code here is an adapted from Genotype.java used in 
 * McDonald-Gibson et al. (2008).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;                           // Part of the model package

import param.Parameters;                  // Uses the model variables

public class Genotype {

    private final double[] mGenes;               // The genotype

    /*
     * Constuct a new genotype and pass the variables
     * @param var The model variables
     */
    public Genotype() {

        mGenes = new double[ 2 ];

        for( int i = 0; i < 2; i++ ) {

            mGenes[ i ] = Parameters.getRandom().nextDouble();    // Initialise genetic information randomly
        }
    }

    /*
     * Construct a new genotype, with the variables and genetic information
     * passed as parameters.
     * @param genes The genetic information
     */
    public Genotype( double[] genes ) {

        mGenes = genes;
    }

    /*
     * Produce a child genotype with potential mutation based on this one.
     * @return The child genotype
     */
    public Genotype reproduce() {

        double[] childGenes = new double[ 2 ];  // new child

        for( int i = 0; i < 2; i++ ) {

            // If there will be mutation
            if( Parameters.getRandom().nextDouble() < Parameters.getMutationRate( i ) ) {

                // Mutate
                childGenes[ i ] = mGenes[ i ] + Parameters.getRandom().nextGaussian() * Parameters.getMutationStandardDeviation( i );

                // Following mutation, ensure alleles remain between 0 and 1
                if( childGenes[ i ] < 0 ) {
                    childGenes[ i ] = 0 - childGenes[ i ];
                } else if( childGenes[ i ] > 1 ) {
                    childGenes[ i ] = 2 - childGenes[ i ];
                }

            } else {
                // Else, return an exact clone of this genotype
                childGenes[ i ] = mGenes[ i ];
            }
        }

        return new Genotype( childGenes );
    }

    /**
     * Getter for property gene.
     *
     * @return Value of property gene.
     */
    public double[] getGene() {
        return mGenes;
    }
}
