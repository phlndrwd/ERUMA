/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Main.java
 *
 * This is the main model class.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;                           // Part of the model package

import plot.Plotter;                     // Plot a graph
import org.jfree.chart.ChartPanel;       // For passing the graph back to the UI
import param.GraphOptions;                 // Uses graph settings
import param.Parameters;                   // Uses variable settings

public class Model {

    private final Plotter mPlotter;            // To plot the graph

    private Resource mResource;           // The model resource
    private Population mPopulation;       // The population of organisms
    private Environment mEnvironment;     // The environment

    private ChartPanel mPanel;           // Contains the graph

    /*
     * Constructor
     */
    public Model() {
        mPlotter = new Plotter();      // Create the graph
    }

    public void mainLoop() {

        // Create the resource
        mResource = new Resource();

        // Create the population
        mPopulation = new Population( mResource );

        // Create the environment
        mEnvironment = new Environment( mPopulation, mResource );

        // Step through each unit of time
        for( int t = 0; t < Parameters.getMaximumTime(); t++ ) {

            // Update the environment at this time step
            mEnvironment.update();

            // Every 100 units of time, plot the data on the graph
            if( t % 100 == 0 ) {

                buildGraph( t );
            }
        }

        // Plot and retrieve the graph when the loop has completed
        setPanel( mPlotter.plotGraph() );
    }

    private void buildGraph( int t ) {

        mEnvironment.calcPlotData();     // Calculate mean statistics

        // If resource forcing should be plotted on the top
        if( GraphOptions.getTopPlot( 0 ) ) {
            mPlotter.addTopData( 0, t, mResource.getResourceNoLife() );
        }

        // If actual resource should be plotted on the top
        if( GraphOptions.getTopPlot( 1 ) ) {
            mPlotter.addTopData( 1, t, mResource.getResourceActual() );
        }

        // If the bounds of habitability should be plotted on the top
        if( GraphOptions.getTopPlot( 2 ) ) {
            mPlotter.addTopData( 2, t, Parameters.getUpperHabitableBound() );
            mPlotter.addTopData( 3, t, Parameters.getLowerHabitableBound() );
        }

        // If the mean adaptation should be plotted on the top
        if( GraphOptions.getTopPlot( 3 ) ) {
            mPlotter.addTopData( 4, t, mEnvironment.getMeanAdaptation() );
        }

        // If mean environmental impact should be plotted on the bottom
        if( GraphOptions.getBottomPlot( 0 ) ) {
            mPlotter.addBottomData( 0, t, mEnvironment.getMeanEffect() );
        }

        // If mean fitness should be plotted on the bottom
        if( GraphOptions.getBottomPlot( 1 ) ) {
            mPlotter.addBottomData( 1, t, mEnvironment.getMeanFitness() );
        }

        // If the number of E alleles should be plotted on the bottom
        if( GraphOptions.getBottomPlot( 2 ) ) {
            mPlotter.addBottomData( 2, t, mEnvironment.getEIncreasingCount() );
        }

        // If the number of e alleles should be plotted on the bottom
        if( GraphOptions.getBottomPlot( 3 ) ) {
            mPlotter.addBottomData( 3, t, mEnvironment.getEDecreasingCount() );
        }
    }

    /**
     * Getter for property mPanel.
     *
     * @return Value of property mPanel.
     */
    public ChartPanel getPanel() {
        return mPanel;
    }

    /**
     * Setter for property mPanel.
     *
     * @param panel New value of property mPanel.
     */
    public void setPanel( ChartPanel panel ) {
        mPanel = panel;
    }
}
