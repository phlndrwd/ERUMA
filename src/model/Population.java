/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Population.java
 *
 * This class represents the population of organisms
 *
 * The code here is adapted from Population.java used in
 * McDonald-Gibson et al. (2008).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;                 // Part of the model package

import java.util.List;         // Represent population as a list
import java.util.ArrayList;    // Define as an array list
import java.util.Random;       // Used for random number generation

public class Population {

    private final Resource mResource;                 // The resource object
    private final List<Organism> mOrganisms;  // The list of organisms

    /*
     * Construct a new population
     * @param resource The resource
     */
    public Population( Resource resource ) {
        mResource = resource;
        mOrganisms = new ArrayList<>();  // The list of organisms
    }

    /*
     * Calculates the strength of the niche construction and updates the resource
     * with the impact
     */
    public void update() {

        double totalEffect = 0;     // The impact of the whole population

        // Iterate through the population
        for( Organism organism: mOrganisms ) {
            // Calculate the impact based on each individual's effect

            totalEffect += organism.getEffect();
        }
        // Update the resource
        mResource.change( totalEffect );
        // Update the fitnesses of the population based on new resource level
        updateFitnesses();
    }

    /*
     * Update the fitness of the whole population
     */
    public void updateFitnesses() {

        for( Organism organism: mOrganisms ) {
            organism.updateFitness( mResource.getResourceActual() );
        }
    }

    /*
     * Retrieve a random member of the population
     * @return The random organism
     */
    public Organism getRandomOrganism() {

        Organism organism = mOrganisms.get( new Random().nextInt( size() ) );

        return organism;
    }

    /*
     * Adds an organism to the population
     * @param organism The organism to add
     */
    public void add( Organism organism ) {
        mOrganisms.add( organism );
    }

    /*
     * Removes an organism from the population
     * @param organism The organism to remove
     */
    public void remove( Organism organism ) {
        mOrganisms.remove( organism );
    }

    /**
     * Getter for population size.
     *
     * @return Value of population size.
     */
    public int size() {
        return mOrganisms.size();
    }

    /**
     * Getter for property mOrganisms.
     *
     * @return Value of property mOrganisms.
     */
    public List<Organism> getOrganisms() {
        return mOrganisms;
    }
}
