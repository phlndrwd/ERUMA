/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Resource.java
 *
 * This class represents the resource that affects the population and is
 * affected by them.
 *
 * The code here is adapted from EnvVar.java and ForcingInfo.java used in
 * McDonald-Gibson et al. (2008).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import param.Parameters;         // Uses model variables

public class Resource {

    private double mResourceActual;         // The resource level with the effects of life
    private double mResourceNoLife;         // The resource level without the effects of life
    private double mCurrentForcingValue;    // The current environmental forcing value

    /*
     * Construct a new resource with the model variables
     * @param var The model variables
     */
    public Resource() {
        // Initialise all to the lowest defined resource value
        mResourceActual = Parameters.getResourceMinimum();
        mResourceNoLife = Parameters.getResourceMinimum();
        mCurrentForcingValue = Parameters.getResourceMinimum();
    }

    /*
     * Update the resource without the effects of life
     */
    public void update() {
        mCurrentForcingValue += Parameters.getResourceIncrement();
        mResourceNoLife += Parameters.getForcingStrength() * ( mCurrentForcingValue - mResourceNoLife );
    }

    /*
     * Once calculated, update the resource with the impact of the population.
     * Represents equations (3) in McDonald-Gibson et al. (2008).
     * @param totalImpact The niche construction of the whole population
     */
    public void change( double totalEffect ) {

        mResourceActual += Parameters.getNicheConstructionStrength() * totalEffect + Parameters.getForcingStrength() * ( mCurrentForcingValue - mResourceActual );

        // Ensure the resource does not go out of range
        if( mResourceActual > Parameters.getResourceMaximum() ) {
            mResourceActual = Parameters.getResourceMaximum();
        }
        if( mResourceActual < Parameters.getResourceMinimum() ) {
            mResourceActual = Parameters.getResourceMinimum();
        }
    }

    /**
     * Getter for property mResourceNoLife.
     *
     * @return Value of property mResourceNoLife.
     */
    public double getResourceNoLife() {
        return mResourceNoLife;
    }

    /**
     * Getter for property mResourceActual.
     *
     * @return Value of property mResourceActual.
     */
    public double getResourceActual() {
        return mResourceActual;
    }
}
