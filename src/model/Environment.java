/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Environment.java
 *
 * This class represents the environment in which the model is situated. It 
 * contains the resource and population. Over which is exercised selection,
 * birth and death.
 *
 * The code here is adapted from Environment.java and GA.java used in
 * McDonald-Gibson et al. (2008).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;                       // Part of the model package

import java.util.Iterator;           // For iteration over the population
import param.Parameters;               // Model variables

public class Environment {

    private final Population mPopulation;  // The population object
    private final Resource mResource;      // The resource object

    private double mMeanAdaptation;  // Average level of adaptation
    private double mMeanFitness;     // Average fitness of the population
    private double mMeanEffect;      // Average impact on the resource
    private int mEDecreasingCount;          // Number of e alleles
    private int mEIncreasingCount;          // Number of E alleles

    /*
     * Constructs a new environment object and initialises variables
     * @param population The population
     * @param resource The resource
     */
    public Environment( Population population, Resource resource ) {

        mPopulation = population;
        mResource = resource;
    }

    /*
     * The point of entry for the model calculations
     */
    public void update() {

        mResource.update();              // Update the resource
        mPopulation.update();            // Update the population
        selection();                    // Perform selection on the population
    }

    /*
     * From here selection, death and birth is actioned over the population
     */
    private void selection() {

        // Calculate number of organisms to die by rounding up
        int numToDie = ( int )Math.ceil( mPopulation.size() * Parameters.getDeathRate() );

        // Run a tournament over the population for the number that will die
        for( int i = 0; i < numToDie; i++ ) {

            tournament();
        }

        // If using a fixed population
        if( Parameters.isFixedPopulation() ) {

            while( mPopulation.size() < Parameters.getCarryingCapacity() ) {

                // Force the population to maximum capacity
                mPopulation.add( new Organism() );
            }

            // Else, using a variable population
        } else {

            while( mPopulation.size() < Parameters.getPopulationMinimum() ) {

                // Force a minimum population
                mPopulation.add( new Organism() );
            }
        }
    }

    /*
     * The steady-state genetic algorithm compares two random organisms
     */
    private void tournament() {

        Organism a;            // The first random member of the population
        Organism b;            // The second random member of the population
        Organism winner;       // The winner of the tournament
        Organism loser;        // The loser of the tournament

        // Retrieve random members of the population
        a = mPopulation.getRandomOrganism();
        b = mPopulation.getRandomOrganism();

        // Determine a winner by comparing fitnesses
        if( a.getFitness() >= b.getFitness() ) {

            winner = a;
            loser = b;

        } else {

            winner = b;
            loser = a;
        }

        if( Parameters.isFixedPopulation() ) {

            // Remove both if their fitnesses are at zero
            if( ( a.getFitness() == 0 ) && ( b.getFitness() == 0 ) ) {

                mPopulation.remove( loser );
                mPopulation.remove( winner );

                // Else, remove the loser and add a child from the winner
            } else {

                mPopulation.remove( loser );
                mPopulation.add( winner.getChild() );
            }
        } else {

            mPopulation.remove( loser );

            // Calculate number of winner's offspring, equation (2)
            int winnerOffspring = ( int )Math.ceil( winner.getFitness() * Math.exp( 1 - ( mPopulation.size() / Parameters.getCarryingCapacity() ) ) );

            // Produce the required amount of children
            for( int i = 0; i < winnerOffspring; i++ ) {

                mPopulation.add( winner.getChild() );
            }
        }
    }

    public void calcPlotData() {

        double totalEffect = 0;         // The total effect of the population
        double totalFitness = 0;        // The total fitness of the population
        double totalAdaptation = 0;     // The total adaptation of the population
        mEIncreasingCount = 0;                 // Initialise E allele counter
        mEDecreasingCount = 0;                 // Initialise e allele counter

        // Iterate through the population in order to calculate totals
        for( Iterator<Organism> it1 = mPopulation.getOrganisms().iterator(); it1.hasNext(); ) {

            Organism organism = it1.next();

            totalEffect += organism.getEffect();
            totalFitness += organism.getFitness();
            totalAdaptation += organism.getAdaptation();

            // Count the number of E/e alleles
            if( organism.isEIncreasing() ) {
                mEIncreasingCount += 1;
            } else {
                mEDecreasingCount += 1;
            }
        }

        // Calculate averages by dividing totals by the population size
        mMeanAdaptation = totalAdaptation / mPopulation.size();
        mMeanFitness = totalFitness / mPopulation.size();
        mMeanEffect = totalEffect / mPopulation.size();
    }

    /**
     * Getter for property mPopulation.
     *
     * @return Value of property mPopulation.
     */
    public Population getPopulation() {
        return mPopulation;
    }

    /**
     * Getter for property mEIncreasingCount.
     *
     * @return Value of property mEIncreasingCount.
     */
    public int getEIncreasingCount() {
        return mEIncreasingCount;
    }

    /**
     * Getter for property mEDecreasingCount.
     *
     * @return Value of property mEDecreasingCount.
     */
    public int getEDecreasingCount() {
        return mEDecreasingCount;
    }

    /**
     * Getter for property mMeanEffect.
     *
     * @return Value of property mMeanEffect.
     */
    public double getMeanEffect() {
        return mMeanEffect;
    }

    /**
     * Getter for property mMeanFitness.
     *
     * @return Value of property mMeanFitness.
     */
    public double getMeanFitness() {
        return mMeanFitness;
    }

    /**
     * Getter for property mMeanAdaptation.
     *
     * @return Value of property mMeanAdaptation.
     */
    public double getMeanAdaptation() {
        return mMeanAdaptation;
    }
}
