/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Organism.java
 *
 * This class represents an individual organism with its genotype that
 * specifies it's particular point of adaptation and the effect it has on the
 * environment. It's fitness is based on its particular optimum and current
 * resource level at the time of calculating.
 *
 * The code here is adapted from Individual.java used in
 * McDonald-Gibson et al. (2008).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;                  // Part of the model package

import param.Parameters;         // Uses model variables

public class Organism {

    private final Genotype mGenotype;  // The genotype

    private boolean mEIncreasing;
    private double mFitness;	// The fitness of this individual
    private double mEffect;	// The effect this individual has on the resource
    private double mAdaptation;  // This particular individuals optimum resource level

    /*
     * Construct a new organism with a random genotype
     */
    public Organism() {

        mGenotype = new Genotype();
        genotypeToPhenotype();
        mFitness = 0;
    }

    /*
     * Construct a new organism with the specified genotype
     * @param genotype The genotype to use
     */
    public Organism( Genotype genotype ) {

        mGenotype = genotype;
        genotypeToPhenotype();
        mFitness = 0;
    }

    /*
     * Translates the genotype into this individuals level of adaptation and the
     * effect it has on the resource
     */
    public final void genotypeToPhenotype() {

        double[] gene = mGenotype.getGene();

        // Find the point of adaptation within the habitable range
        mAdaptation = gene[ 0 ] * ( Parameters.getUpperHabitableBound() - Parameters.getLowerHabitableBound() ) + Parameters.getLowerHabitableBound();

        if( Parameters.isFixedImpact() == true ) {      // If using fixed impact
            if( gene[ 1 ] >= 0.5 ) {     // Alter effect and set allele type
                mEffect = 1.0;
                mEIncreasing = true;

            } else if( gene[ 1 ] < 0.5 ) {
                mEffect = -1.0;
                mEIncreasing = false;
            }
        } else {                        // Else, leave effect and set allele type
            mEffect = ( 2 * gene[ 1 ] ) - 1;     // Find effect if using continous impact

            if( gene[ 1 ] >= 0.5 ) {
                mEIncreasing = true;
            } else if( gene[ 1 ] < 0.5 ) {
                mEIncreasing = false;
            }
        }
    }

    /*
     * Calculate fitness based on passed resource level, represents equation (1)
     * in McDonald-Gibson et al. (2008).
     * @param resource The resource level on which the fitness is based
     */
    public void updateFitness( double resource ) {

        mFitness = Math.max( 0, 1 - Math.pow( ( mAdaptation - resource ) / Parameters.getFitnessFunctionWidth(), 2 ) );
    }

    /*
     * Return a child of this organism
     * @return The child
     */
    public Organism getChild() {

        return new Organism( mGenotype.reproduce() );
    }

    /**
     * Getter for property mEIncreasing.
     *
     * @return Value of property mEIncreasing.
     */
    public boolean isEIncreasing() {
        return mEIncreasing;
    }

    /**
     * Getter for property mFitness.
     *
     * @return Value of property mFitness.
     */
    public double getFitness() {
        return mFitness;
    }

    /**
     * Getter for property mEffect.
     *
     * @return Value of property mEffect.
     */
    public double getEffect() {
        return mEffect;
    }

    /**
     * Getter for property mAdaptation.
     *
     * @return Value of property mAdaptation.
     */
    public double getAdaptation() {
        return mAdaptation;
    }
}
