/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Plotter.java
 *
 * This class makes use of the JFreeChart chart library in order to build a
 * stacked line graph based on the settings passed to it. It has methods to add
 * data to the various data sets that will eventually form the basis for the
 * graph, and a method to construct the ChartPanel object that is passed back 
 * to the main interface for display.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package plot;           // Member of the plot package

// JFreeChart objects
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import param.GraphOptions;                 // Used to handle graph variables

public class Plotter {

    private CombinedDomainXYPlot mCombinedPlot;     // Pulls top and bottom plots together
    private XYSeries mSeriesTop[];                  // The data set array for the top graph
    private XYSeries mSeriesBottom[];               // The data set array for the bottom graph
    private XYSeriesCollection mCollectionTop;      // Collection of top data
    private XYSeriesCollection mCollectionBottom;   // Collection of bottom data
    private XYDataset mDatasetTop;                  // Top graph data to be plotted
    private XYDataset mDatasetBottom;               // Bottom graph data to be plotted
    private XYItemRenderer mRendererTop;            // Handles top graph colours
    private XYItemRenderer mRendererBottom;         // Handles bottom graph colours
    private NumberAxis mRangeAxisTop;               // Top Y axis
    private NumberAxis mRangeAxisBottom;            // Bottom Y axis
    private XYPlot mSubplotTop;                     // Top graph plot
    private XYPlot mSubplotBottom;                  // Bottom graph plot
    private ChartPanel mPanel;                      // The final graph displayed
    private JFreeChart mChart;                      // The graph generated

    /*
     * The constructor to initialise the graph
     * @param graphSettings Passed graph settings
     */
    public Plotter() {
        initGraph();
    }

    /*
     * Initialises the variables and sets up the graph for plotting
     */
    private void initGraph() {

        mSeriesTop = new XYSeries[ 5 ];
        mSeriesTop[ 0 ] = new XYSeries( "Forcing (P)" );
        mSeriesTop[ 1 ] = new XYSeries( "Resource (R)" );
        mSeriesTop[ 2 ] = new XYSeries( "Upper Bound" );
        mSeriesTop[ 3 ] = new XYSeries( "Lower Bound" );
        mSeriesTop[ 4 ] = new XYSeries( "Niche Construction (A)" );

        mSeriesBottom = new XYSeries[ 4 ];
        mSeriesBottom[ 0 ] = new XYSeries( "Mean Impact (I)" );
        mSeriesBottom[ 1 ] = new XYSeries( "Fitness (F)" );
        mSeriesBottom[ 2 ] = new XYSeries( "E Individuals" );
        mSeriesBottom[ 3 ] = new XYSeries( "e Individuals" );

        mCombinedPlot = new CombinedDomainXYPlot( new NumberAxis( "Time" ) );
        mChart = new JFreeChart( mCombinedPlot );

        mCollectionTop = new XYSeriesCollection();
        mCollectionBottom = new XYSeriesCollection();

        mRendererTop = new StandardXYItemRenderer();
        mRendererBottom = new StandardXYItemRenderer();

        mRangeAxisTop = new NumberAxis( "Resource" );

        if( GraphOptions.getBottomPlot( 0 ) ) {
            mRangeAxisBottom = new NumberAxis( "Niche Construction Outputs" );
        } else if( GraphOptions.getBottomPlot( 1 ) ) {
            mRangeAxisBottom = new NumberAxis( "Fitness (F)" );
        } else if( GraphOptions.getBottomPlot( 2 ) ) {
            mRangeAxisBottom = new NumberAxis( "Number of E Individuals" );
        } else if( GraphOptions.getBottomPlot( 3 ) ) {
            mRangeAxisBottom = new NumberAxis( "Number of e Individuals" );
        }
    }

    /*
     * Adds data to the top plot
     */
    public void addTopData( int index, double x, double y ) {
        mSeriesTop[ index ].add( x, y );
    }

    /*
     * Adds data to the bottom plot
     */
    public void addBottomData( int index, double x, double y ) {
        mSeriesBottom[ index ].add( x, y );
    }

    /*
     * Builds and renders the graph according to the passed settings
     */
    public ChartPanel plotGraph() {

        int count = 0;

        for( int i = 0; i < 2; i++ ) {
            if( GraphOptions.getTopPlot( i ) ) {
                mCollectionTop.addSeries( mSeriesTop[ i ] );
                mRendererTop.setSeriesPaint( count, GraphOptions.getTopLineColour( i ) );
                count += 1;
            }
        }

        if( GraphOptions.getTopPlot( 2 ) ) {
            mCollectionTop.addSeries( mSeriesTop[ 2 ] );
            mCollectionTop.addSeries( mSeriesTop[ 3 ] );
            mRendererTop.setSeriesPaint( count, GraphOptions.getTopLineColour( 2 ) );
            count += 1;
            mRendererTop.setSeriesPaint( count, GraphOptions.getTopLineColour( 2 ) );
            count += 1;
        }

        if( GraphOptions.getTopPlot( 3 ) ) {
            mCollectionTop.addSeries( mSeriesTop[ 4 ] );
            mRendererTop.setSeriesPaint( count, GraphOptions.getTopLineColour( 3 ) );
            count += 1;
        }

        for( int i = 0; i < 4; i++ ) {
            if( GraphOptions.getBottomPlot( i ) ) {
                mCollectionBottom.addSeries( mSeriesBottom[ i ] );
                mRendererBottom.setSeriesPaint( 0, GraphOptions.getBottomLineColour( i ) );
            }
        }

        mDatasetTop = mCollectionTop;
        mDatasetBottom = mCollectionBottom;

        mSubplotTop = new XYPlot( mDatasetTop, null, mRangeAxisTop, mRendererTop );
        mSubplotBottom = new XYPlot( mDatasetBottom, null, mRangeAxisBottom, mRendererBottom );

        mSubplotTop.setBackgroundPaint( GraphOptions.getGraphColour( 1 ) );
        mSubplotBottom.setBackgroundPaint( GraphOptions.getGraphColour( 1 ) );

        mSubplotTop.setRangeAxisLocation( AxisLocation.BOTTOM_OR_LEFT );
        mSubplotBottom.setRangeAxisLocation( AxisLocation.TOP_OR_LEFT );

        mCombinedPlot.setGap( 10.0 ); // Set the gap between each graph.
        mCombinedPlot.add( mSubplotTop );
        mCombinedPlot.add( mSubplotBottom );
        mChart.setBackgroundPaint( GraphOptions.getGraphColour( 0 ) );
        mPanel = new ChartPanel( mChart, true, true, true, true, false );

        return mPanel;
    }
}
