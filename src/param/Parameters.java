/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Parameters.java
 *
 * This class stores all of the parameters configured from the interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package param;                            // Part of the model package

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.Random;

public class Parameters {

    // Model parameters
    private static int mResourceMinimum;                // Minimum (start) resource value
    private static int mResourceMaximum;                // Maximum (end) resource value
    private static int mCarryingCapacity;               // Carrying capacity (max population size)
    private static int mPopulationMinimum;              // Minimum population size
    private static double mForcingStrength;             // External forcing strength
    private static double mNicheConstructionStrength;   // Niche construction strength
    private static int mMaximumTime;                    // Maximum time for the simulation
    private static boolean mIsFixedPopulation;          // Fixed or variable population

    // Organism parameters
    private static int mLowerHabitableBound;            // Lower bound of habitability
    private static int mUpperHabitableBound;            // Upper bound of habitability
    private static final double[] mMutationRate = new double[ 2 ]; // Mutation rate
    private static final double[] mMutationStandardDeviation = new double[ 2 ]; // Mutation standard deviation
    private static double mDeathRate;                   // The death rate
    private static double mFitnessFunctionWidth;        // The width of the fitness function
    private static boolean mIsFixedImpact;              // Fixed or continuous impact

    private static int mResourceRange;                  // Calculated value; resource range
    private static double mResourceIncrement;           // Calculated value; forcing increment

    private static final Random mRandom = new Random();  // Random number object

    public static void recalculateParameters() {
        mResourceRange = mResourceMaximum - mResourceMinimum;
        mResourceIncrement = ( double )mResourceRange / mMaximumTime;
    }

    /**
     * Getter for property mMaximumTime.
     *
     * @return Value of property mMaximumTime.
     */
    public static int getMaximumTime() {
        return mMaximumTime;
    }

    /**
     * Setter for property mMaximumTime.
     *
     * @param maximumTime New value of property mMaximumTime.
     */
    public static void setMaximumTime( int maximumTime ) {
        mMaximumTime = maximumTime;
        recalculateParameters();
    }

    /**
     * Getter for property mResourceMinimum.
     *
     * @return Value of property mResourceMinimum.
     */
    public static int getResourceMinimum() {
        return mResourceMinimum;
    }

    /**
     * Setter for property mResourceMinimum.
     *
     * @param resourceMinimum New value of property mResourceMinimum.
     */
    public static void setResourceMinimum( int resourceMinimum ) {
        mResourceMinimum = resourceMinimum;
        recalculateParameters();
    }

    /**
     * Getter for property mResourceMaximum.
     *
     * @return Value of property mResourceMaximum.
     */
    public static int getResourceMaximum() {
        return mResourceMaximum;
    }

    /**
     * Setter for property mResourceMaximum.
     *
     * @param resourceMaximum New value of property mResourceMaximum.
     */
    public static void setResourceMaximum( int resourceMaximum ) {
        mResourceMaximum = resourceMaximum;
        recalculateParameters();
    }

    /**
     * Getter for property mCarryingCapacity.
     *
     * @return Value of property mCarryingCapacity.
     */
    public static int getCarryingCapacity() {
        return mCarryingCapacity;
    }

    /**
     * Setter for property mCarryingCapacity.
     *
     * @param carryingCapacity New value of property mCarryingCapacity.
     */
    public static void setCarryingCapacity( int carryingCapacity ) {
        mCarryingCapacity = carryingCapacity;
    }

    /**
     * Getter for property mForcingStrength.
     *
     * @return Value of property mForcingStrength.
     */
    public static double getForcingStrength() {
        return mForcingStrength;
    }

    /**
     * Setter for property mForcingStrength.
     *
     * @param forcingStrength New value of property mForcingStrength.
     */
    public static void setForcingStrength( double forcingStrength ) {
        mForcingStrength = forcingStrength;
    }

    /**
     * Getter for property mNicheConstructionStrength.
     *
     * @return Value of property mNicheConstructionStrength.
     */
    public static double getNicheConstructionStrength() {
        return mNicheConstructionStrength;
    }

    /**
     * Setter for property mNicheConstructionStrength.
     *
     * @param nicheConstructionStrength New value of property
     * mNicheConstructionStrength.
     */
    public static void setNicheConstructionStrength( double nicheConstructionStrength ) {
        mNicheConstructionStrength = nicheConstructionStrength * 10E-5;
    }

    /**
     * Getter for property mLowerHabitableBound.
     *
     * @return Value of property mLowerHabitableBound.
     */
    public static int getLowerHabitableBound() {
        return mLowerHabitableBound;
    }

    /**
     * Setter for property mLowerHabitableBound.
     *
     * @param lowerHabitableBound New value of property mLowerHabitableBound.
     */
    public static void setLowerHabitableBound( int lowerHabitableBound ) {
        mLowerHabitableBound = lowerHabitableBound;
    }

    /**
     * Getter for property mUpperHabitableBound.
     *
     * @return Value of property mUpperHabitableBound.
     */
    public static int getUpperHabitableBound() {
        return mUpperHabitableBound;
    }

    /**
     * Setter for property mUpperHabitableBound.
     *
     * @param upperHabitableBound New value of property mUpperHabitableBound.
     */
    public static void setUpperHabitableBound( int upperHabitableBound ) {
        mUpperHabitableBound = upperHabitableBound;
    }

    /**
     * Getter for property mDeathRate.
     *
     * @return Value of property mDeathRate.
     */
    public static double getDeathRate() {
        return mDeathRate;
    }

    /**
     * Setter for property mDeathRate.
     *
     * @param deathRate New value of property mDeathRate.
     */
    public static void setDeathRate( double deathRate ) {
        mDeathRate = deathRate;
    }

    /**
     * Getter for property mFitnessFunctionWidth.
     *
     * @return Value of property mFitnessFunctionWidth.
     */
    public static double getFitnessFunctionWidth() {
        return mFitnessFunctionWidth;
    }

    /**
     * Setter for property mFitnessFunctionWidth.
     *
     * @param fitnessFunctionWidth New value of property mFitnessFunctionWidth.
     */
    public static void setFitnessFunctionWidth( double fitnessFunctionWidth ) {
        mFitnessFunctionWidth = fitnessFunctionWidth;
    }

    /**
     * Indexed getter for property mMutationRate.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static double getMutationRate( int index ) {
        return mMutationRate[ index ];
    }

    /**
     * Indexed setter for property mMutationRate.
     *
     * @param index Index of the property.
     * @param mutationRate New value of the property at <CODE>index</CODE>.
     */
    public static void setMutationRate( int index, double mutationRate ) {
        mMutationRate[ index ] = mutationRate;
    }

    /**
     * Indexed getter for property mMutationStandardDeviation.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static double getMutationStandardDeviation( int index ) {
        return mMutationStandardDeviation[ index ];
    }

    /**
     * Indexed setter for property mMutationStandardDeviation.
     *
     * @param index Index of the property.
     * @param mutationStandardDeviation New value of the property at
     * <CODE>index</CODE>.
     */
    public static void setMutationStandardDeviation( int index, double mutationStandardDeviation ) {
        mMutationStandardDeviation[ index ] = mutationStandardDeviation;
    }

    /**
     * Getter for property mResourceRange.
     *
     * @return Value of property mResourceRange.
     */
    public static int getResourceRange() {
        return mResourceRange;
    }

    /**
     * Setter for property mResourceRange.
     *
     * @param resourceRange New value of property mResourceRange.
     */
    public static void setResourceRange( int resourceRange ) {
        mResourceRange = resourceRange;
    }

    /**
     * Getter for property mResourceIncrement.
     *
     * @return Value of property mResourceIncrement.
     */
    public static double getResourceIncrement() {
        return mResourceIncrement;
    }

    /**
     * Setter for property mResourceIncrement.
     *
     * @param resourceIncrement New value of property mResourceIncrement.
     */
    public static void setResourceIncrement( double resourceIncrement ) {
        mResourceIncrement = resourceIncrement;
    }

    /**
     * Getter for property mIsFixedImpact.
     *
     * @return Value of property mIsFixedImpact.
     */
    public static boolean isFixedImpact() {
        return mIsFixedImpact;
    }

    /**
     * Setter for property mIsFixedImpact.
     *
     * @param fixedImpact New value of property mIsFixedImpact.
     */
    public static void setFixedImpact( boolean fixedImpact ) {
        mIsFixedImpact = fixedImpact;
    }

    /**
     * Getter for property mIsFixedPopulation.
     *
     * @return Value of property mIsFixedPopulation.
     */
    public static boolean isFixedPopulation() {
        return mIsFixedPopulation;
    }

    /**
     * Setter for property mIsFixedPopulation.
     *
     * @param isFixedPopulation New value of property mIsFixedPopulation.
     */
    public static void setIsFixedPopulation( boolean isFixedPopulation ) {
        mIsFixedPopulation = isFixedPopulation;
    }

    /**
     * Getter for property mPopulationMinimum.
     *
     * @return Value of property mPopulationMinimum.
     */
    public static int getPopulationMinimum() {
        return mPopulationMinimum;
    }

    /**
     * Setter for property mPopulationMinimum.
     *
     * @param populationMinimum New value of property populationMin.
     */
    public static void setPopulationMinimum( int populationMinimum ) {
        mPopulationMinimum = populationMinimum;
    }

    public static Random getRandom() {
        return mRandom;
    }

    public static void centreWindow( Window frame ) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = ( int )( ( dimension.getWidth() - frame.getWidth() ) / 2 );
        int y = ( int )( ( dimension.getHeight() - frame.getHeight() ) / 2 );
        frame.setLocation( x, y );
    }

}
