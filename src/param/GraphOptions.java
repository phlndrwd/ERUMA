/*
 *   ERUMA - An individual-based Java/NetBeans Gaian model implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GraphOptions.java
 *
 * This class stores all of the graph plotting options configured from the
 * interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package param;          // Part of the model package

import java.awt.Color;  // Uses colours

public class GraphOptions {

    private final static boolean[] mTopPlots = new boolean[ 4 ];        // The top graph lines
    private final static boolean[] mBottomPlots = new boolean[ 4 ];     // The bottom graph lines
    private final static Color[] mTopLineColours = new Color[ 4 ];      // The top line colours
    private final static Color[] mBottomLineColours = new Color[ 4 ];   // The bottom line colours
    private final static Color[] mGraphColours = new Color[ 2 ];        // The graph colours

    /**
     * Indexed getter for property mTopPlots.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static boolean getTopPlot( int index ) {
        return mTopPlots[ index ];
    }

    /**
     * Indexed setter for property mTopPlots.
     *
     * @param index Index of the property.
     * @param topPlots New value of the property at <CODE>index</CODE>.
     */
    public static void setTopPlot( int index, boolean topPlots ) {
        mTopPlots[ index ] = topPlots;
    }

    /**
     * Indexed getter for property mBottomPlots.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static boolean getBottomPlot( int index ) {
        return mBottomPlots[ index ];
    }

    /**
     * Indexed setter for property mBottomPlots.
     *
     * @param index Index of the property.
     * @param bottomPlots New value of the property at <CODE>index</CODE>.
     */
    public static void setBottomPlot( int index, boolean bottomPlots ) {
        mBottomPlots[ index ] = bottomPlots;
    }

    /**
     * Indexed getter for property mTopLineColours.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static Color getTopLineColour( int index ) {
        return mTopLineColours[ index ];
    }

    /**
     * Indexed setter for property mTopLineColours.
     *
     * @param index Index of the property.
     * @param topLineColour New value of the property at <CODE>index</CODE>.
     */
    public static void setTopLineColour( int index, Color topLineColour ) {
        mTopLineColours[ index ] = topLineColour;
    }

    /**
     * Indexed getter for property mBottomLineColours.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static Color getBottomLineColour( int index ) {
        return mBottomLineColours[ index ];
    }

    /**
     * Indexed setter for property mBottomLineColours.
     *
     * @param index Index of the property.
     * @param bottomLineColour New value of the property at <CODE>index</CODE>.
     */
    public static void setBottomLineColour( int index, Color bottomLineColour ) {
        mBottomLineColours[ index ] = bottomLineColour;
    }

    /**
     * Indexed getter for property mGraphColours.
     *
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static Color getGraphColour( int index ) {
        return mGraphColours[ index ];
    }

    /**
     * Indexed setter for property mGraphColours.
     *
     * @param index Index of the property.
     * @param graphColour New value of the property at <CODE>index</CODE>.
     */
    public static void setGraphColour( int index, Color graphColour ) {
        mGraphColours[ index ] = graphColour;
    }
}
